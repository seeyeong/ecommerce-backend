<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/user/{id?}', 'UserController@index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/products', 'ProductController');

Route::get('/logo', 'ProductController@getImage');

Route::get('/apartment', 'ApartmentController@index');
Route::get('/updateImagesUrl', 'ProductController@updateImagesUrl');

Route::get('/inarray', function () {
    $id = 6;
    $colors = [1, 4, 5];

    if (in_array($id, $colors)) {
        echo "selected";
    }
});
