<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StorageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = ['64GB', '128GB', '256GB', '512GB', '1TB'];
        $storages = [];
        foreach ($arr as $storage) {
            $storages[] = ['name' => $storage];
        }
        DB::table('storage')->insert($storages);
    }
}
