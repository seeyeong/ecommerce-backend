<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductColorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('product_colors')->insert([
            [
                'product_id' => 1,
                'colors_id' => 1
            ],
            [
                'product_id' => 1,
                'colors_id' => 2
            ],
            [
                'product_id' => 1,
                'colors_id' => 3
            ],
            [
                'product_id' => 2,
                'colors_id' => 2
            ],
            [
                'product_id' => 2,
                'colors_id' => 3
            ],
        ]);
    }
}
