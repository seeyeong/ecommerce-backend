<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('products')->insert(
            [
                [
                    "category_id" => 1,
                    "name" => "iPhone 11 Pro",
                    "price" => 4899.0,
                    "qty" => 100,
                    "description" => "All-new triple-camera system (Ultra Wide, Wide, Telephoto)\nUp to 20 hours of video playback1\nWater resistant to a depth of 4 metres for up to 30 minutes2\n5.8″ or 6.5″ Super Retina XDR display3",
                    "imgUrl" => "https://www.apple.com/v/iphone/home/af/images/overview/compare/compare_iphone_11_pro__fvqwhr4dkhiu_large_2x.jpg"
                ],
                [
                    "category_id" => 1,
                    "name" => "iPhone 11",
                    "price" => 3399.0,
                    "qty" => 100,
                    "description" => "All-new dual-camera system (Ultra Wide, Wide)\nUp to 17 hours of video playback1\nWater resistant to a depth of 2 metres for up to 30 minutes2\n6.1″ Liquid Retina HD display3",
                    "imgUrl" => "https://www.apple.com/v/iphone/home/af/images/overview/compare/compare_iphone_11_pro__fvqwhr4dkhiu_large_2x.jpg"
                ],
                [
                    "category_id" => 1,
                    "name" => "iPhone XR",
                    "price" => 2899.0,
                    "qty" => 100,
                    "description" => "ingle-camera system\n(Wide)\nUp to 16 hours of video playback1\nWater resistant to a depth of 1 metre for up to 30 minutes2\n6.1″ Liquid Retina HD display3",
                    "imgUrl" => "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/iphone-xr-white-select-201809?wid=940&hei=1112&fmt=png-alpha&qlt=80&.v=1551226036668"
                ],
                [
                    "category_id" => 2,
                    "name" => "macbook",
                    "price" => 10499.0,
                    "qty" => 100,
                    "description" => "2.6GHz 6-core 9th-generation Intel Core i7 processor\nTurbo Boost up to 4.5GHz\nAMD Radeon Pro 5300M with 4GB of GDDR6 memory\n16GB 2666MHz DDR4 memory\n512GB SSD storage¹\n16-inch Retina display with True Tone\nTouch Bar and Touch ID\nFour Thunderbolt 3 ports",
                    "imgUrl" => "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/mbp16touch-space-select-201911_GEO_MY?wid=904&hei=840&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1573242759842",
                ],
                [
                    "category_id" => 2,
                    "name" => "macbook",
                    "price" => 5499.0,
                    "qty" => 100,
                    "description" => "1.4GHz quad-core 8th-generation Intel Core i5 processor\nTurbo Boost up to 3.9GHz\nIntel Iris Plus Graphics 645\n8GB 2133MHz LPDDR3 memory\n128GB SSD storage¹\nRetina display with True Tone\nTouch Bar and Touch ID\nTwo Thunderbolt 3 ports1",
                    "imgUrl" => "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/mbp13touch-space-select-201807_GEO_MY?wid=904&hei=840&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1531167638725",
                ],
                [
                    "category_id" => 2,
                    "name" => "macbook",
                    "price" => 7849.0,
                    "qty" => 100,
                    "description" => "2.4GHz quad-core 8th-generation Intel Core i5 processor\nTurbo Boost up to 4.1GHz\nIntel Iris Plus Graphics 655\n8GB 2133MHz LPDDR3 memory\n256GB SSD storage¹\nRetina display with True Tone\nTouch Bar and Touch ID\nFour Thunderbolt 3 ports",
                    "imgUrl" => "https://store.storeimages.cdn-apple.com/8756/as-images.apple.com/is/mbp13touch-space-select-201807_GEO_MY?wid=904&hei=840&fmt=jpeg&qlt=80&op_usm=0.5,0.5&.v=1531167638725",
                ],
                [
                    "category_id" => 2,
                    "name" => "macbook",
                    "price" => 4699.0,
                    "qty" => 100,
                    "description" => "13.3-inch Retina display1\n2-core Intel Core i5 processor\nUp to 16GB memory\nUp to 1TB storage2\nUp to 12 hours battery life3\nTouch ID\nBacklit keyboard",
                    "imgUrl" => "https://www.apple.com/v/mac/home/am/images/overview/compare/macbook_air__csdfieli984m_medium_2x.jpg",
                ],
                [
                    "category_id" => 3,
                    "name" => "iPad Pro",
                    "price" => 3499.0,
                    "qty" => 100,
                    "description" => "12.9″ and 11″\nLiquid Retina display\nA12X Bionic chip\nFace ID\nUp to 1TB storage\nSupport for Apple Pencil\n(2nd generation)\nSupport for Smart Keyboard Folio",
                    "imgUrl" => "https://www.apple.com/v/ipad/home/aw/images/overview/compare_ipad_pro__diziempxq7o2_large_2x.png",
                ]
            ]
        );
    }
}
