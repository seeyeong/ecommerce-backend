<?php

use Illuminate\Database\Seeder;

class ProductStorageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_storage')->insert([
            [
                'product_id' => 1,
                'storage_id' => 1
            ],
            [
                'product_id' => 1,
                'storage_id' => 2
            ],
            [
                'product_id' => 1,
                'storage_id' => 3
            ],
            [
                'product_id' => 2,
                'storage_id' => 2
            ],
            [
                'product_id' => 2,
                'storage_id' => 3
            ],
        ]);
    }
}
