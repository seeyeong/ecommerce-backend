<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ColorsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('colors')->insert([
            [
                'name' => 'Silver'
            ],
            [
                'name' => 'Rose Gold'
            ],
            [
                'name' => 'Space Grey'
            ],
            [
                'name' => 'Midnigth Green'
            ],
        ]);
    }
}
