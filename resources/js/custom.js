$("#multi-select-inputColors").select2({
    // dropdownParent: $('#productModal'),
    placeholder: "Select a state",
    allowClear: true
});

$("#multi-select-inputStorages").select2({
    // dropdownParent: $('#productModal'),
    placeholder: "Select a state",
    allowClear: true
});

$(".custom-file-input").on("change", function() {
    //get the file name
    var fileName = $(this).val();
    console.log(fileName);
    //replace the "Choose a file" label
    $(this)
        .next(".custom-file-label")
        .html(fileName);
});

function handleDelete($id) {
    var token = $("meta[name='csrf-token']").attr("content");
    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d33d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!"
    }).then(result => {
        if (result.value) {
            $.ajax({
                url: "products/" + $id,
                type: "DELETE",
                data: {
                    _token: token
                },
                success: function(e) {
                    console.log("it Works", e);
                    Toast.fire({
                        icon: "success",
                        title: "Item has been deleted!"
                    });
                }
            });
        }
    });
}

const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: toast => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    }
});

$(".select-multiple").select2();
