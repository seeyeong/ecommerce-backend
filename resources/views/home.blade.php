@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @php
        $flds = ['id','imgUrl','name','description','qty','price','created_at','updated_at'];
        @endphp
        <div class="col-dm-8 col-lg-12">
            <div class="card">
                <div class="card-header">
                    Products
                </div>
                <div class="card-body">
                    <div class="toolbar">
                        <a href="/products/create" class="btn btn-primary">Add</a>
                        {{-- <button class="btn btn-primary" data-toggle="modal" data-target="#productModal"><span><i
                                    class="fas fa-plus"></i> Add</span></button> --}}
                    </div>
                    <div class="table-responsive">
                        <table data-toggle="table" data-pagination="true" data-search="true" data-show-refresh="true"
                            data-show-columns="true" data-toolbar-align="right" data-toolbar=".toolbar"
                            class="table-striped">
                            <thead>
                                <tr>
                                    @foreach ($flds as $fld)
                                    <th scope="col">{{$fld}}</th>
                                    @endforeach
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (isset($products))
                                @foreach ($products as $product)
                                <tr>
                                    @foreach ($flds as $fld)
                                    @if ($fld == 'imgUrl')
                                    <td><img style="width: 80px; height: 80px" src="{{$product->$fld}}"
                                            alt="{{$product->$fld}}"></td>
                                    @else
                                    <td>{{$product->$fld}}</td>
                                    @endif
                                    @endforeach
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-secondary dropdown-toggle" type="button"
                                                id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                Action
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <a class="dropdown-item" href="/products/{{$product->id}}/edit">Edit</a>
                                                <button class="dropdown-item delete-product-btn"
                                                    onclick="handleDelete({{$product->id}})">Delete</button>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if (isset($products))
    <div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="productModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable" role="document">
            <div class="modal-content">
                <form method="POST" action="/products" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @php
                        /**
                        * span:string => class for col
                        * label:string => fom input label
                        * id:string => id for input
                        * textual:string => textual for switch [input, select, select2, file, textarea]
                        * type:string => type for input
                        * name:string => input name for form
                        * require:boolean [optional] => true to make input required
                        * model:App\Model => laravel model for [select, select2]
                        * placeholder:string [optional]
                        *
                        * note:
                        * default textual => input
                        * select,select2 required model for display select options
                        */
                        $flds = [
                        ['span'=>'col-md-12','label'
                        =>'Name','id'=>'inputName','textual'=>'input','type'=>'text','name'=>'name','required'=>true],
                        ['span'=>'col-md-12','label'
                        =>'Description','id'=>'inputDescription','textual'=>'textarea','row'=>5,'col'=>20,'type'=>'text','name'=>'description','required'=>true],
                        ['span'=>'col-md-4','label'
                        =>'Categories','model'=>$categories
                        ,'id'=>'inputCategories','textual'=>'select','type'=>'text','name'=>'categories'],
                        ['span'=>'col-md-4','label'
                        =>'Qty','id'=>'inputQty','textual'=>'input','type'=>'number','name'=>'qty','defaultValue'=>0],
                        ['span'=>'col-md-5','label'
                        =>'Price','id'=>'inputPrice','textual'=>'','type'=>'number','name'=>'price',
                        'placeholder'=>'fill in price here','defaultValue'=>0],
                        ['span'=>'col-md-12','label'
                        =>'Colors','model'=>
                        $colors,'id'=>'inputColors','textual'=>'select2','type'=>'text','name'=>'colors[]'],
                        ['span'=>'col-md-12','label'
                        =>'Storages','model'=>
                        $storages,'id'=>'inputStorages','textual'=>'select2','type'=>'text','name'=>'storages[]'],
                        ['span'=>'col-md-6','label'
                        =>'Image','id'=>'inputImgUrl','textual'=>'file','type'=>'file','name'=>'imgUrl']
                        ]
                        @endphp

                        <div class="form-row">
                            @foreach ($flds as $fld)
                            @switch($fld['textual'])
                            @case('file')
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                                </div>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01"
                                        name="{{$fld['name']}}" aria-describedby="inputGroupFileAddon01">
                                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                                </div>
                            </div>
                            @break
                            @case('select')
                            @php
                            $options = $fld['model'];
                            @endphp
                            <div class="form-group">
                                <label for="{{$fld['id']}}">{{$fld['label']}}</label>
                                <select class="form-control" id="{{$fld['id']}}" style="width: 100%"
                                    name={{$fld['name']}}>
                                    @foreach ($options as $option)
                                    <option value="{{$option['id']}}">{{$option['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @break
                            @case('select2')
                            @php
                            $options = $fld['model'];
                            @endphp
                            <div class="form-group {{$fld['span']}}">
                                <label for="{{$fld['id']}}">{{$fld['label']}}</label>
                                <select class="form-control" style="width: 100%" id="multi-select-{{$fld['id']}}"
                                    name="{{$fld['name']}}" multiple="multiple">
                                    @foreach ($options as $option)
                                    <option value="{{$option['id']}}">{{$option['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @break
                            @case('textarea')
                            <div class="form-group {{$fld['span']}}">
                                <label for="{{$fld['id']}}">{{$fld['label']}}</label>
                                <textarea class="form-control" id="{{$fld['id']}}"
                                    rows="{{isset($fld['row']) ? $fld['row'] : 10}}"
                                    cols="{{isset($fld['col']) ? $fld['col'] : 20}}" name="{{$fld['name']}}"></textarea>
                            </div>
                            @break
                            @default
                            <div class="form-group {{$fld['span']}}">
                                <label for="{{$fld['id']}}">{{$fld['label']}}</label>
                                <input type="{{$fld['type']}}" name="{{$fld['name']}}" class="form-control"
                                    id="{{$fld['id']}}"
                                    placeholder="{{isset($fld['placeholder']) ? $fld['placeholder'] : $fld['name'] }}"
                                    value="{{isset($fld['defaultValue']) ? $fld['defaultValue'] : null }}"
                                    required='{{isset($fld['required'])}}'>
                            </div>
                            @endswitch
                            @endforeach
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif

@endsection