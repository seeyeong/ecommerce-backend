@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <h1>Axios Apartment</h1>
    <pre>
      Lorem, ipsum dolor sit amet consectetur adipisicing elit. Temporibus natus, corrupti totam hic ex maiores earum nihil blanditiis quae quibusdam enim est tempora a sunt iure architecto. Ducimus, aliquam iusto?
    </pre>
  </div>
  <div class="row">
    <div class="col">
      <h2>Rooms</h2>
      <table class="table">
        <thead>
          <th>id</th>
          <th>room type</th>
          <th>qty</th>
          <th>price</th>
        </thead>
        <tbody>
          @foreach ([1,2,3,4,5] as $item)
          <tr>
            <td>{{$item}}</td>
            <td>room type</td>
            <td>{{$item}}</td>
            <td>{{$item * 100}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection