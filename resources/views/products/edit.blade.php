@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Edit Product {{$product->id}}</h1>
  <div class="row">
    <div class="col">
      <form method="POST" action="/products/{{$product->id}}" enctype="multipart/form-data">
        @csrf
        @method('PUT')

        <div class="row">
          <div class="form-group col">
            <label for="productCategory">Category</label>
            <select
              class="form-control {{$errors->first('categories') ? 'is-invalid' : ''}} {{old('categories') ? 'is-valid' : ''}}"
              name="categories" id="productCategory">
              @foreach ($categories as $category)
              <option value="{{$category->id}}" {{$product->category_id === $category->id ? 'selected' : ''}}>
                {{$category->name}}</option>
              @endforeach
            </select>
            @error('categories')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group col">
            <label for="productName ">Name</label>
            <input type="text"
              class="form-control {{$errors->first('name') ? 'is-invalid' : '' }} {{old('name') ? 'is-valid' : ''}}"
              name="name" id="productName" placeholder="Enter product name"
              value="{{old('name') ? old('name') : $product->name}}">

            @error('name')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>
        </div>

        <div class="row">
          <div class="form-group col-6">
            <label for="productDescription">Description</label>
            <textarea
              class="form-control {{$errors->first('description') ? 'is-invalid' : ''}} {{old('description') ? 'is-valid' : ''}}"
              name="description" id="productDescription" cols="30" rows="5">{{$product->description}}</textarea>
            @error('description')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group col-3">
            <label for="productColors">Colors</label>
            <select multiple="multiple"
              class="select-multiple form-control {{$errors->first('colors') ? 'is-invalid' : ''}} {{old('colors') ? 'is-valid' : ''}}"
              name="colors[]" id="productColors">
              @foreach ($colors as $color)
              <option value="{{$color->id}}"
                {{in_array($color->id, $product->colors->pluck('id')->toArray()) ? 'selected' : ''}}>
                {{$color->name}}</option>
              @endforeach
            </select>
            @error('colors')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group col-3">
            <label for="productStorages">Storages</label>
            <select multiple="multiple"
              class="select-multiple form-control {{$errors->first('storages') ? 'is-invalid' : ''}} {{old('storages') ? 'is-valid' : ''}}"
              name="storages[]" id="productStorages">
              @foreach ($storages as $storage)
              <option value="{{$storage->id}}"
                {{in_array($storage->id, $product->storages->pluck('id')->toArray()) ? 'selected' : ''}}>
                {{$storage->name}}</option>
              @endforeach
            </select>
            @error('storages')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>
        </div>

        <div class="row">
          <div class="col">
            <div class="row">
              <div class="form-group col">
                <label for="productQty">Qty</label>
                <input type="number"
                  class="form-control {{$errors->first('qty') ? 'is-invalid' : ''}} {{old('qty') ? 'is-valid' : ''}}"
                  name="qty" id="productQty" value="{{$product->qty}}">
                @error('qty')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="row">
              <div class="form-group col">
                <label for="productPrice">Price</label>
                <input type="number"
                  class="form-control {{$errors->first('price') ? 'is-invalid' : ''}} {{old('price') ? 'is-valid' : ''}}"
                  name="price" id="productPrice" value="{{$product->price}}">
                @error('price')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
          </div>
          <div class="col">
            <div class="row">
              <img src="{{asset($product->imgUrl)}}" width="85px" height="85px" alt="{{$product->imgUrl}}">
            </div>
            <div class="row">
              <div class="form-group col">
                <label for="productImage">Image</label>
                <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                  </div>
                  <div class="custom-file">
                    <input type="file"
                      class="custom-file-input {{$errors->first('imgUrl') ? 'is-invalid' : ''}} {{old('imgUrl') ? 'is-valid' : ''}}"
                      name="imgUrl" id="productImage" aria-describedby="inputGroupFileAddon01"
                      value="{{old('imgUrl')}}">
                    <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col text-right">
            <button type="submit" class="btn btn-primary ">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection