@extends('layouts.app')

@section('content')
<div class="container">
  <h1>Create Product</h1>
  <div class="row">
    <div class="col">
      <form method="POST" action="/products" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="form-group col">
            <label for="productCategory">Category</label>
            <select
              class="form-control {{$errors->first('categories') ? 'is-invalid' : ''}} {{old('categories') ? 'is-valid' : ''}}"
              name="category_id" id="productCategory">
              @foreach ($categories as $category)
              <option value="{{$category->id}}">{{$category->name}}</option>
              @endforeach
            </select>
            @error('categories')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group col">
            <label for="productName">Name</label>
            <input type="text"
              class="form-control {{$errors->first('name') ? 'is-invalid' : ''}} {{old('name') ? 'is-valid' : ''}}"
              name="name" id="productName" placeholder="Enter product name" value="{{old('name')}}">
            @error('name')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>
        </div>

        <div class="row">
          <div class="form-group col-6">
            <label for="productDescription">Description</label>
            <textarea
              class="form-control {{$errors->first('description') ? 'is-invalid' : ''}} {{old('description') ? 'is-valid' : ''}}"
              name="description" id="productDescription" cols="30" rows="5">{{old('description')}}</textarea>
            @error('description')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group col-3">
            <label for="productColors">Colors</label>
            <select
              class="select-multiple form-control {{$errors->first('colors') ? 'is-invalid' : ''}} {{old('colors') ? 'is-valid' : ''}}"
              id="productColors" name="colors[]" multiple="multiple">
              @foreach ($colors as $color)
              <option value="{{$color->id}}"
                {{ old('colors') ? in_array( $color->id,old('colors') ) ? "selected" : '' : ''}}>{{$color->name}}
              </option>
              @endforeach
            </select>
            @error('colors')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group col-3">
            <label for="productStorages">Storages</label>
            <select multiple="multiple"
              class="select-multiple form-control {{$errors->first('storages') ? 'is-invalid' : ''}} {{old('storages') ? 'is-valid' : ''}}"
              name="storages[]" id="productStorages">
              @foreach ($storages as $storage)
              <option value="{{$storage->id}}" {{ old('storages') ? in_array( $storage->id,
                                      old('storages') ) ? "selected" : '' : ''}}>{{$storage->name}}</option>
              @endforeach
            </select>
            @error('storages')
            <div class="invalid-feedback">{{ $message }}</div>
            @enderror
          </div>
        </div>

        <div class="row">
          <div class="col">
            <div class="row">
              <div class="form-group col">
                <label for="productQty">Qty</label>
                <input type="number"
                  class="form-control {{$errors->first('qty') ? 'is-invalid' : ''}} {{old('qty') ? 'is-valid' : ''}}"
                  name="qty" id="productQty" value="{{old('qty')}}">
                @error('qty')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
            <div class="row">
              <div class="form-group col">
                <label for="productPrice">Price</label>
                <input type="number"
                  class="form-control {{$errors->first('price') ? 'is-invalid' : ''}} {{old('price') ? 'is-valid' : ''}}"
                  name="price" id="productPrice" value="{{old('price')}}">
                @error('price')
                <div class="invalid-feedback">{{ $message }}</div>
                @enderror
              </div>
            </div>
          </div>
          <div class="form-group col">
            <label for="productImage">Image</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
              </div>
              <div class="custom-file">
                <input type="file"
                  class="custom-file-input {{$errors->first('imgUrl') ? 'is-invalid' : ''}} {{old('imgUrl') ? 'is-valid' : ''}}"
                  name="imgUrl" id="productImage" aria-describedby="inputGroupFileAddon01" value="{{old('imgUrl')}}">
                <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col text-right">
            <button type="submit" class="btn btn-primary ">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection