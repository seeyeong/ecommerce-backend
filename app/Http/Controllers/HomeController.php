<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Color;
use App\Storage;
use App\Products;
use Illuminate\Http\Request;
use App\Http\Controllers\ProductController;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $products = ProductController::index();
        $colors = Color::all();
        $storages = Storage::all();
        $categories = Categories::all();
        return view('home', compact('products', 'colors', 'storages', 'categories'));
    }
}
