<?php

namespace App\Http\Controllers;

use App\Color;
use App\Products;
use App\Categories;
use App\ProductColors;
use Illuminate\Http\File;
use App\Storage as Storages;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{

    public static function index()
    {
        $products = Products::with('colors', 'categories', 'storages')->latest()->get();
        // $product = Products::with('colors', 'categories', 'storages')->orderBy('id', 'DESC')->get();
        return $products;
    }

    public function show(Products $product)
    {
        return $product->with('colors', 'categories', 'storages')->first();
    }

    public function store()
    {
        //save product
        $product = Products::create(request()->validate([
            'name' => 'required',
            'category_id' => 'required',
            'description' => 'required',
            'qty' => 'required',
            'price' => 'required',
        ]));

        $imgPath = '';
        //check has file then save imgUrl
        if (request()->hasFile('imgUrl')) {
            $imgPath = request('imgUrl')->store('images');
            $product->imgUrl = $imgPath;
            $product->save();
        }

        if ($product) {
            $product->colors()->attach(request('colors'));
            $product->storages()->attach(request('storages'));
        }

        return redirect('home')->with('status', 'Product saved!');
    }

    public function create()
    {
        $categories = Categories::all();
        $colors = Color::all();
        $storages = Storages::all();
        $data = [
            'categories' => $categories,
            'colors' => $colors,
            'storages' => $storages,
        ];
        return view('products.create', $data);
    }

    public function edit(Products $product)
    {
        //view edit page
        $colors = Color::all();
        $storages = Storages::all();
        $categories = Categories::all();
        $product = $product->with('colors', 'categories', 'storages')->where('id', $product->id)->first();
        return view('products.edit', compact('product', 'categories', 'colors', 'storages'));
    }

    public function update(Products $product)
    {
        //update existing product

        //check has file then save imgUrl
        $product->update(request()->validate([
            'name' => 'required',
            'description' => 'required',
            'qty' => 'required',
            'price' => 'required',
        ]));

        if (request()->hasFile('imgUrl')) {
            $imgPath = request('imgUrl')->store('images');
            $product->imgUrl = $imgPath;
            $product->save();
        }

        $product->colors()->sync(request('colors'));
        $product->storages()->sync(request('storages'));

        return redirect()->back()->withInput()->with('status', 'Product updated!');
    }

    public function destroy(Products $product)
    {
        return $product->delete();
    }

    public function updateImagesUrl()
    {
        $products = Products::where('imgUrl', '!=', '')->get();
        foreach ($products as $product) {
            preg_match('/http.*\/(.*\.\w+)/', $product->imgUrl, $matches);
            if ($matches) {
                $file = md5($matches[1]);
                $ext = pathinfo(parse_url($matches[0], PHP_URL_PATH), PATHINFO_EXTENSION);
                Storage::disk('images')->put("$file.{$ext}", file_get_contents($matches[0]));

                $product->imgUrl = "images/$file.{$ext}";
                $product->save();
            }
        }
    }

    public function getProductByCategory($id)
    {
        return Products::where('category_id', $id)->with('colors', 'categories', 'storages')->get();
    }

    public function getImage()
    {
        $contents = Storage::disk('public')->get('react.png');
        return $contents;
    }
}
