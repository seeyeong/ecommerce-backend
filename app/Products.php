<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public $timestamps = true;

    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $guarded = [];

    public function colors()
    {
        return $this->belongsToMany('App\Color', 'product_colors', 'product_id', 'colors_id');
    }

    public function categories()
    {
        return $this->belongsTo('App\Categories', 'category_id');
    }

    public function storages()
    {
        return $this->belongsToMany('App\Storage', 'product_storage', 'product_id', 'storage_id');
    }

    public function productColors()
    {
        return $this->hasMany('App\ProductColors', 'product_id');
    }

    public function productStorages()
    {
        return $this->hasMany('App\ProductStorage', 'product_id');
    }
}
