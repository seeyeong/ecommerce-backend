<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductColors extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function colorsable()
    {
        return $this->morphTo();
    }

    public $timestamps = true;
}
